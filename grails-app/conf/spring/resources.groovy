import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver

// Place your Spring DSL code here
beans = {
    freeMarkerConfigurer(FreeMarkerConfigurer) {
        // 模板直接放src/main/webapp下，存在安全问题
        // templateLoaderPath = "/"

        // 模板文件存放在src/main/webapp/WEB-INF/templates下面
        templateLoaderPath = "/WEB-INF/templates/"
    }
    viewResolver(FreeMarkerViewResolver) {
        cache = "false"
        prefix = "bin/freemarker"
        suffix = ".ftl"
        exposeSpringMacroHelpers = "true"
        // requestContextAttribute = "request"
    }
}
