<!DOCTYPE html>
<html class="loginHtml">
<head>
    <meta charset="utf-8">
    <title>test page.</title>
</head>
<body>
用户名：${user!}
<br/>
系统版本：${os!}
<br/>
JDK版本：${java_version!}
<br/>
Tomcat版本：${tomcat_version!}

<#if tomcat_version>
<br/>
    tomcat版本为：${tomcat_version}

</#if>
</body>
</html>